import { AxiosRequestConfig } from 'axios';
import { makeAuthorizationHeaderValue } from '../utils';
import { Interceptor } from '../types';

const makeAuthorizationInterceptor: (token?: string) => Interceptor = (token?: string) => (
  config: AxiosRequestConfig,
): AxiosRequestConfig => {
  if (config.headers && config.headers.Authorization) {
    return config;
  }

  if (!token || !token.length) {
    return config;
  }

  return {
    ...config,
    headers: {
      ...config.headers,
      Authorization: makeAuthorizationHeaderValue(token),
    },
  };
};

export default makeAuthorizationInterceptor;
