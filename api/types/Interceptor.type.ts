import { AxiosRequestConfig } from 'axios';

export type Interceptor = (config: AxiosRequestConfig) => AxiosRequestConfig
