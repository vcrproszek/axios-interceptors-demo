import axios, {
  AxiosRequestConfig,
  AxiosInstance,
} from 'axios';
import { AuthorizationService } from 'global/services';
import { makeAuthorizationInterceptor } from './interceptors';

const apiClient: (config?: AxiosRequestConfig) => AxiosInstance = (config?: AxiosRequestConfig) => {
  const client: AxiosInstance = axios.create({
    baseURL: (config && config.baseURL) || process.env.REACT_APP_DEFAULT_BASE_URL,
    responseType: 'json',
  });

  client.interceptors.request.use(makeAuthorizationInterceptor(AuthorizationService.getToken()));

  return client;
};

export const api: AxiosInstance = apiClient({
  baseURL: process.env.REACT_APP_CUSTOM_BASE_API,
});
