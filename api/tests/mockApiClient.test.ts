import { apiV2 } from '../client';

jest.mock('claim-ui.package', () => ({
  ClaimUIPackage: {
    authorize: jest.fn().mockReturnValue(new Promise(() => {})),
    getToken: jest.fn().mockReturnValue('token'),
  },
}));

jest.mock('../client', () => ({
  apiV2: {
    get: jest.fn().mockReturnValue(new Promise(() => {})),
  },
}));

const pingMonitor: Function = async (query: string) => {
  const url: string = `https://any-valid-url/${query}`;

  return apiV2.get(url);
};

describe('mockApiClient', () => {
  it('successfully get data from mocked API client', async () => {
    const data: {message: string} = {
      message: 'All good in the hood',
    };
    expect(pingMonitor('get')).resolves.toEqual(data);

    expect(apiV2.get).toHaveBeenCalledWith('https://any-valid-url/get');
  });
});
