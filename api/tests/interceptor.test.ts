import { AxiosRequestConfig } from 'axios';
import { ClaimUIPackage } from 'claim-ui.package';
import { apiV2 } from '../client';
import { makeAuthorizationInterceptor } from '../interceptors';
import { makeAuthorizationHeaderValue } from '../utils';
import { Interceptor } from '../types';

jest.mock('claim-ui.package', () => ({
  ClaimUIPackage: {
    authorize: jest.fn().mockReturnValue(new Promise(() => {})),
    getToken: jest.fn().mockReturnValue('token'),
  },
}));

jest.mock('../client', () => ({
  apiV2: {
    get: jest.fn().mockReturnValue(new Promise(() => {})),
    interceptors: {
      request: {
        use: jest.fn(),
        eject: jest.fn(),
      },
      response: {
        use: jest.fn(),
        eject: jest.fn(),
      },
    },
  },
}));
const authToken: string = `valid-${ClaimUIPackage.getToken()}`;
const authorizationHeaderValue: string = makeAuthorizationHeaderValue(authToken);

describe('auth interceptor', () => {
  it('should use auth interceptor once', () => {
    apiV2.interceptors.request.use(makeAuthorizationInterceptor());
    expect(apiV2.interceptors.request.use).toHaveBeenCalledTimes(1);
  });
  it('should add token to the Authorization header', () => {
    const auth: (config: AxiosRequestConfig)
      => AxiosRequestConfig = makeAuthorizationInterceptor(authToken);
    const config: AxiosRequestConfig = auth({});

    expect(config.headers.Authorization).toBe(authorizationHeaderValue);
  });
  it('should not add token to the Authorization header if exists', () => {
    const authInterceptor: Interceptor = makeAuthorizationInterceptor('new-token');
    const config: AxiosRequestConfig = authInterceptor({
      headers: { Authorization: authorizationHeaderValue },
    });

    expect(config.headers.Authorization).toBe(authorizationHeaderValue);
  });
  it('if token is empty Authorization should be empty', () => {
    const authInterceptor: Interceptor = makeAuthorizationInterceptor();
    const config: AxiosRequestConfig = authInterceptor({});

    expect(config.headers).toBe(undefined);
  });
});
