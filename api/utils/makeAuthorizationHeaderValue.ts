const makeAuthHeaderValue: (token?: string) => string = (token?: string) => `Bearer ${token}`;

export default makeAuthHeaderValue;
